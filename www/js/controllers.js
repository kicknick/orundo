function IndexCtrl($scope, $http, $location) {
    token.validate($http, function successCallback() {
        $http({
            method: 'GET',
            url: APP_SERVER_API_URL + '/project/'
        }).success(function (data, status, headers, config) {
                $scope.projects = data.results;
            });
    }, function failCallback() {
        $location.path("/login/index");
    });
}

function LoginCtrl($scope, $http, $location, $route, $routeParams) {
    token.validate($http, function successCallback() {
        if ($routeParams.redirect_url) {
            $location.path('/' + $routeParams.redirect_url);
        } else {
            $location.path("/tasks");
        }
    }, function failCallback() {
        $scope.submit = function () {
            $http({
                method: 'POST',
                url: APP_SERVER_URL + '/api-token-auth/',
                data: {
                    username: $scope.email,
                    password: $scope.password
                }
            }).success(function (data, status, headers, config) {
                    token.save(data.token);
                    if ($routeParams.redirect_url) {
                        $location.path('/' + $routeParams.redirect_url);
                    } else {
                        $route.reload();
                    }
                }).
                error(function (data, status, headers, config) {
                    $scope.error = "Wrong password!";
                });
        };
    });
}

function RegisterCtrl($scope, $http, $location) {
    token.validate($http, function successCallback() {
        $location.path("/tasks");
    }, function failCallback() {
        $scope.submit = function () {
            $http({
                method: 'POST',
                url: APP_SERVER_URL + '/register/',
                data: {
                    username: $scope.email,
                    title: $scope.title,
                    password: $scope.password
                }
            }).success(function (data, status, headers, config) {
                    token.save(data.token);
                    $location.path('/tasks');
                }).
                error(function (data, status, headers, config) {
                    $scope.error = "Error!";
                });
        };
    });
}

function TasksCtrl($scope, $http, $location) {
    $http({
        method: 'GET',
        url: APP_SERVER_API_URL + '/task/'
    }).success(function (data, status, headers, config) {

        }).error(function (data, status, headers, config) {

        });
}

function ProjectsCtrl($scope, $http, $location) {
    token.validate($http, function successCallback() {
        $http({
            method: 'GET',
            url: APP_SERVER_API_URL + '/project/'
        }).success(function (data, status, headers, config) {
                $scope.projects = data.results;
            }).error(function (data, status, headers, config) {
                $location.path("/login/projects");
            });
    }, function failCallback() {

    });
}

function ProjectCtrl($scope, $http, $location, $routeParams) {
    token.validate($http, function successCallback() {
        $http({
            method: 'GET',
            url: APP_SERVER_API_URL + '/project/' + $routeParams.projectId + '/'
        }).success(function (data, status, headers, config) {
                var project = data;
                $http({
                    method: 'GET',
                    url: APP_SERVER_API_URL + '/organization/' + project.organization + '/'
                }).success(function (data, status, headers, config) {
                        project.organization = data;
                        $scope.project = project;
                    });
                project.users.forEach(function(){

                });
            }).error(function (data, status, headers, config) {
                $location.path("/login/projects/");
            });
    }, function failCallback() {

    });
}