var APP_SERVER_URL = 'http://orundo.herokuapp.com';
var APP_SERVER_API_URL = 'http://orundo.herokuapp.com/api';

var orundo = angular.module('orundo', ['ngRoute']).
    config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                when('/index', {
                    templateUrl: 'partials/index.html',
                    controller: IndexCtrl
                }).
                when('/login', {
                    templateUrl: 'partials/login.html',
                    controller: LoginCtrl
                }).
                when('/login/:redirect_url*', {
                    templateUrl: 'partials/login.html',
                    controller: LoginCtrl
                }).
                when('/register', {
                    templateUrl: 'partials/register.html',
                    controller: RegisterCtrl
                }).
                when('/tasks', {
                    templateUrl: 'partials/tasks.html',
                    controller: TasksCtrl
                }).
                when('/projects/:projectId', {
                    templateUrl: 'partials/project.html',
                    controller: ProjectCtrl
                }).
                when('/projects', {
                    templateUrl: 'partials/projects.html',
                    controller: ProjectsCtrl
                }).
                otherwise({
                    redirectTo: '/index'
                });
        }]);

var token = {
    save: function (token) {
        sessionStorage.token = angular.toJson(token);
    },
    get: function () {
        return angular.fromJson(sessionStorage.token);
    },
    validate: function ($http, successCallback, failCallback) {
        $http.defaults.headers.common.Authorization = 'Token ' + this.get();
        $http({
            method: 'GET',
            url: APP_SERVER_URL + '/api/'
        }).success(function (data, status, headers, config) {
                successCallback();
            }).
            error(function (data, status, headers, config) {
                if (status == 401) {
                    sessionStorage.token = null;
                    $http.defaults.headers.common.Authorization = null;
                    failCallback();
                }
                else {
                    successCallback();
                }
            });
    }
};